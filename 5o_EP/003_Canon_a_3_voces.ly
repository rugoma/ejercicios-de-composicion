\version "2.18.2"
\language "espanol"

\header {
  title = "Ejercicio 003: Canon"
  subtitle = "Canon a 3 voces"
  copyright = "Creative Commons 4.1 - BY"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key sol \minor
  \time 4/4
  \tempo 4=100
}

canon = {
  do2 re8 mib8 re4 sol,8 sol4 \tuplet 3/2 {sol8[ la sib]} mib8 do4~
  do4 \tuplet 3/2{sib8 la sol} sol'4 mib8 do8
  re2 do8 sol'4 r8 % Fin primera frase.

  la,2 sib8 la8 sol4 fas8. sol16 ~sol4 \tuplet 3/2 {la8 la8 la8} do4
  \tuplet 3/2 {mi8 mi mi} fas4 \tuplet 3/2 {si8 si si} do4
  \tuplet 3/2 {si16 la sol} \tuplet 3/2 {la sol fas} do'2 % Fin 2ª frase

  si8 la~ la8 fa re sol4 re8 do4 \tuplet 3/2 {mib8 fa fas} sol2 r4 % Fin 3a frase

  %% Sección 4ª y final del canon
  do,2 re8 mib8 re4 la2 sib8 la sol4 fa8 fa4 \tuplet 3/2 {fa8 sol la} re8 sib4
  %% Compas final
  mib4 re4 sol,4. r8
}

vozsoprano = \relative do'' {
  \global
  % La música continúa aquí.
  r1 r1 \canon
  % Coda final
  do8 re16 mib16 re4 \tuplet 3/2 {fa8 mib re} sib4
  mib4 re4 sol,2\fermata
}

voztenor = \relative do' {
  \global
  \dynamicUp
  % La música continúa aquí.
  r1 \canon
  do4 fa,2 re'4
  % Coda final
  mib4 sib la re sol, la sib2\fermata
}

vozbajo = \relative do {
  \global
  \dynamicUp
  % La música continúa aquí.
  \canon
  la4 re4 sib4 re4
  la2 sib2
  % Coda final
  sol2 la4 fa'4
  mib4 fa sol2\fermata
}

sopranoVoicePart = \new Staff \with {
  instrumentName = "Soprano"
  midiInstrument = "choir aahs"
} { \vozsoprano }

tenorVoicePart = \new Staff \with {
  instrumentName = "Tenor"
  midiInstrument = "choir aahs"
} { \clef "treble_8" \voztenor }

bassVoicePart = \new Staff \with {
  instrumentName = "Bajo"
  midiInstrument = "choir aahs"
} { \clef bass \vozbajo }


\score {
  <<
    \sopranoVoicePart
    \tenorVoicePart
    \bassVoicePart
  >>
  \layout { }
  \midi {
    \tempo 4=100
  }
}