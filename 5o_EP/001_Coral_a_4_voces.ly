\version "2.18.2"
\language "espanol"

\header {
  title = "Un título"
  instrument = "Coral a 4 voces"
  composer = "R. Gómez Antolí"
  copyright = "Creative Commons - BY - 4.1"
}

global = {
  \key la \major
  \numericTimeSignature
  \time 3/4
}

sopranoVoice = \relative do'' {
  \global
  \dynamicUp
  % La música continúa aquí.
  % A1
  % I - III - VI - V - IV - IV - II- I
  la2 dos4 fas4 mi2 res8[ mi] res4 si dos2.
  % A2
  r4 fas8 re mi4 re4 fas2 sols2 re4 dos2.
  % B1
  r4 dos4 la4 mis'4 fas4 res si4 la2 si4 dos2 \breathe
  % B2 - Se vuelve a la tonalidad mayor
  % IV - VI - VIII/V (cambio a LaM) - I - V - VI - V - I
  \tuplet 3/2 {si8 la sols} re'2 mi4 la2 sols8 fas8 mi2 la,8 sols la2
  \breathe
  % A1
  la2 dos4 fas4 mi2 res8[ mi] res4 si dos2.
  % A2
  % IV - III - IV - VI - V/V - V - I
  r4 fas8 re mi4 re4 fas2 si4 sols2 dos,8 si dos2
}

altoVoice = \relative do' {
  \global
  \dynamicUp
  % La música continúa aquí.
  % A1
  mi2~ mi4 dos' si2 la4 fas2 mi2.
  % A2
  r4 la4 mi4 fas4 la2 dos2 si4 mi,2.
  % B1 - Modulación a fa#m
  % I - III - IV - VI - VII - I - IV - V (Semicadencia)
  r4 fas4 sols8 la si4 la4 res, mis4 fas2 re4 mi2 \breathe
  % B2
  re4 fas2 sols4 mi2 la4 sols2 mi2 r4
  % A1
  mi2~ mi4 dos' si2 la4 fas2 mi2.
  % A2
  r4 la4 sols4 fas4 la2 fas4 si2 mi,2.
  %%dos2 si4 mi,2.
}

tenorVoice = \relative do' {
  \global
  \dynamicUp
  % La música continúa aquí.
  % A1
  dos2 mi4 la, sols2 fas8 mi la4 res, dos2 r4
  % A2
  % IV - III - IV - V/V - V - VI - I
  re4 dos8 si dos4 re4 si2 mi4 \tuplet 3/2 {re8 dos si} sols'4 la2 r4
  % B1 - Melodía en la contralto
  la2 mi4 sols4 fas4 la4 sols4 dos2 fas4 sols2 \breathe
  % B2 - Melodía en la soprano
  fas,4 la2 si4 dos2 la4 si2 dos2. \breathe
  % A1
  dos2 mi4 la, sols2 fas8 mi la4 res, dos2 r4
  % A2 - Melodía en esta voz
  % IV - III - IV - VI - V/V - V - I
  fas4 dos8 si dos4 la'4 fas2 re4 si'2 la8 sols la2
}

bassVoice = \relative do {
  \global
  \dynamicUp
  % La música continúa aquí.
  % A1
  la2 sols4 fas fas'4 mi res4 fas2 la2.
  % A2
  fas4 fas, sols4 la4 dos2 si4 fas4 fas'4 mi2 r4
  % B1 - Modulación a fa#m
  % I - III - IV - VI - VII - I - IV - V (Semicadencia)
  % Melodía en contraalto.
  fas2 la4  mis4 la4 fas4 mis4 la2 si8 si, mi2
  \breathe
  % B2 - Melodía en soprano
  % IV - VI - VIII/V (cambio a LaM) - I - V - VI - V - I
  si4 fas2 mi4 dos2 fas4 mi2 dos2 r4
  % A1
  la'2 sols4 fas fas'4 mi res4 fas2 la2.
  % A2
  % IV - III - IV - VI - V/V - V - I
  re,2 dos4 re4 la2 si4 mi2 la2.
}

sopranoVoicePart = \new Staff \with {
  instrumentName = "Soprano"
  midiInstrument = "choir aahs"
} { \sopranoVoice }

altoVoicePart = \new Staff \with {
  instrumentName = "Alto"
  midiInstrument = "choir aahs"
} { \altoVoice }

tenorVoicePart = \new Staff \with {
  instrumentName = "Tenor"
  midiInstrument = "choir aahs"
} { \clef "treble_8" \tenorVoice }

bassVoicePart = \new Staff \with {
  instrumentName = "Bajo"
  midiInstrument = "choir aahs"
} { \clef bass \bassVoice }

\score {
  <<
    \sopranoVoicePart
    \altoVoicePart
    \tenorVoicePart
    \bassVoicePart
  >>
  \layout { }
  \midi {
    \tempo 4=100
  }
}
