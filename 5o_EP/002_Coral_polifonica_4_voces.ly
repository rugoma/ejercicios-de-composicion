\version "2.18.2"
\language "espanol"

\paper {
  #(set-paper-size "a4" 'landscape)
}

\header {
  title = "Ejercicio 002: coral polifónica"
  instrument = "Coral modal a 4 voces"
  composer = "R. Gómez Antolí"
  copyright = "Creative Commons - BY - 4.1"
}

global = {
  \key la \minor
  \time 4/4
  \tempo 4=100
}

vozsoprano = \relative do'' {
  \global
  % La música continúa aquí.
  r2 re4 fa4 re4 fa8 sol8 sol,4 mi' fa4. si8 la4 r4
  la4 sol fa mi re do re mi ~ mi8 do mi4 r2 
  %%% Fin sección A, empieza sección B
  do4 re16 mi sol4. fa8 mi8 re4 % Fin parte imitativa, comienza 2º motivo
  mi fas sol re mi8 mi re4 do si la4. si16 do re4 % Fin 2o motivo
  r4 si8 la8 sol4 la16 si16 la8~ la8 si16 do si4 sol8 fa16 sol si4 \breathe
  %%% Fin sección B, empieza sección B.
  mi,4 re'4 la4 sol4 fa mi8 mi'8 la,4 si sol si do la fa' do mi si % 1er motivo
  r4 re sol fa do mi re r4
  %%% Fin sección B, empieza sección A
  la4 do8 re8 re,4 si' do4. fa8 mi4 r4 % 1er motivo melódico
  fa8 mi4 fa8 mi4 re8 do8~ do4. si8 la4. r8  % 2o motivo melódico
  si8 la4 si8 do4 la4~ la4 re16 do si8\> la2\! % 3er motivo melódico. 
}

vozcontraalto = \relative do'' {
  \global
  % La música continúa aquí.
  la4 do8 re8 re,4 si' do4. fa8 mi4 r4 % 1er motivo melódico
  fa8 mi4 fa8 mi4 re8 do8~ do4. si8 la4. r8  % 2o motivo melódico
  si8 la4 si8 do4 la4~ la4 re16 do si8 la4 r4 % 3er motivo melódico. 
  %%% Fin sección A, empieza sección B
  mi4 re4 la'4 sol4 fa mi la si sol si do la fa do mi si' % 1er motivo
  r4 re, sol fa do mi re r4
  %%% Fin sección B, empieza sección B
  do4 re16 mi sol4. fa8 mi8 re4 % Fin parte imitativa, comienza 2º motivo
  mi fas sol re mi8 mi re4 do si la4. si16 do re4 % Fin 2o motivo
  r4 si'8 la8 sol4 la16 si16 la8~ la8 si16 do si4 sol8 fa16 sol si4 \breathe
  %%% Fin sección B, empieza sección A.
  r2 la4 do8 re8 re,4 si' do4. fa8 mi4 r4 % 1er motivo melódico
  la,4 sol fa mi re do % 2o motivo, acompañamiento
  mi8 re4 mi8 fa4 do4~ do8 mi2. r8 % 3er motivo imitado a la 5J descendente
}

voztenor = \relative do' {
  \global
  \dynamicUp
  % La música continúa aquí.
  r2 la4 do8 re8 re,4 si' do4. fa8 mi4 r4 % 1er motivo melódico
  la4 sol fa mi re do % 2o motivo, acompañamiento
  mi8 re4 mi8 fa4 do4~ do8 mi4. r4 % 3er motivo imitado a la 5J descendente
  %%% Fin sección A, la sección B empieza en anacrusa en esta voz.
  r8 sol,8~ sol4 la16 si re4. do8 si la4 sols fas4 mi16 re sol8~ sol r8 %1er mot. melódico
  mi8 re8 do4 re16 mi16 fa8~ fa8 sol16 la16 sol4 mi8 re16 mi sol4 % 2o motivo melódico
  r4 re'8[ do si la] do[ si la sol] mi'[ re8 sol,16 la16] si4 r8
  %%% Fin sección B, empieza sección B
  do2 sol2 fa'2 re2 mi2 fa2 fa,2 la2 si2 re2 mi8 do sol8. la16 si2 
  %%% Fin sección B, empieza sección A
  r1 si2 la2 fa'2 mi4 mi, fa2 re'4 re, fa2 mi2 re8 si8 do8. re16\> do2\! % Fin sección A
}

vozbajo = \relative do {
  \global
  \dynamicUp
  % La música continúa aquí.
  % la2 fa2 
  r1 si2 la2 fa2 mi2 fa2 re'2 fa2 mi2 re8 si8 do8. re16 do4 r4 % Fin sección A
  do2 sol2 fa'2 re2 mi2 fa2 fa,2 la2 si2 re2 mi8 do sol8. la16 si2 \breathe
  %%% Fin sección B, empieza sección B
  sol4 la16 si re4. do8 si la4 sols fas4 mi16 re sol8~ sol r8 %1er mot. melódico
  mi8 re8 do4 re16 mi16 fa8~ fa8 sol16 la16 sol4 mi8 re16 mi sol4 % 2o motivo melódico
  r4 re'8[ do si la] do[ si la sol] mi'[ re8 sol,16 la16] si4 r8
  %%% Fin sección B, empieza sección A
  r2 la4 do8 re8 re,4 si' do4. fa8 mi4 r4 % 1er motivo melódico
  la4 sol fa mi re do % 2o motivo, acompañamiento
  mi8 re4 mi8 fa4 do4~ do8 mi2. r8 % 3er motivo imitado a la 5J descendente
}

sopranoVoicePart = \new Staff \with {
  instrumentName = "Soprano"
  midiInstrument = "choir aahs"
} { \vozsoprano }

altoVoicePart = \new Staff \with {
  instrumentName = "Alto"
  midiInstrument = "choir aahs"
} { \vozcontraalto }

tenorVoicePart = \new Staff \with {
  instrumentName = "Tenor"
  midiInstrument = "choir aahs"
} { \clef "treble_8" \voztenor }

bassVoicePart = \new Staff \with {
  instrumentName = "Bajo"
  midiInstrument = "choir aahs"
} { \clef bass \vozbajo }


\score {
  <<
    \sopranoVoicePart
    \altoVoicePart
    \tenorVoicePart
    \bassVoicePart
  >>
  \layout { }
  \midi {
    \tempo 4=100
  }
}
