\version "2.19.83"
\language "espanol"

\header {
  title = "Invención en FaM"
  composer = "R. Gómez Antolí"
  copyright = "CC-By 4.1"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key fa \major
  \time 3/4
  \tempo "Andante"
}

%% Estructura general de la pieza
% Ritornello (en T y termina en cadencia)
% Episodio (progresiones, cadencia en tonalidad de destino)
% Ritornello (relativoT)
% Episodio (progresiones)
% Ritornello (dominante)
% Episodio (progresiones)
% Ritornello (en T y con pedal de T)

flauta = \relative do'' {
   \global
   % Motivo principal
   % I - V - I
   fa,16 la' do,8 mi16 re do8 la fa
   % I - V - I
   do16 fa do mi do re sol8 fa4
   %fa do do'8 la16 sol fa8 %la'8 fa4
   % Imitación en el bajo
   r8 do' sol'16 fa mi si do4
   mi8 sol re16 do sib8 sol do
   % Cadencia
   do4
   % Empieza 1er episodio
   % Motivo B
   sib8^"Ep. 1. Motivo B" la16 sol mi4
   sol8 la sol16 la sol4.
   % Fin de motivo B, empieza progresión
   la16 sib sib la la sol sol16 la
   % Modulación a rem y cadencia
   fa4 
   sol8 fa dos'8 re16 mi la,8. r16
   % Ritornello en rem
   re16^"Ritornello" fa' la,8 dos16 si la8 fa re
   la16 re la dos la si mi8 re4
   % I - V - I
   fa,8 sib dos16 re mi8 sol16 re fa8
   mi8 sib16 dos re8 dos re8 la
   % Episodio 2
   % Progresiones en contrapunto imitativo.
   re8[^"Ep. 2." la] sib[ fa] si4 sol mi'8 sol,
   % Motivo B
   do mi sol si16 re, mi8 fa sol do
   % Cadencia perfecta final episodio
   re,8 la fa' re mi4
   % Ritornello 3
   r8 mi re si do4
   % Estrechos y cadencia
   si16^"Estrechos" mi' sol,8 si16 la fa8 mi re
   do re mi
   % Motivo A
   do16 mi' sol,8 si16 la 
   % Cadencia
   sol8 mi re4 re
   mi8[ do]
   % Episodio 3
   la'8 fa mi re
   do4 si16 re si8 re sol
   % Progresiones
   la8 fa re fa sib! fa
   mi16 sol mi sib' la8 do fa,8 la
   %mi'16 do fa,16 mi do'8 la16 fa sib8 re16 do
   %do8 la mi4 la8 r
   % Ritornello 4 en FaM
   % Motivo principal
   % I - V - I
   fa,16 la' do,8 mi16 re do8 la fa
   % I - V - I
   do16 fa do mi do re sol8 fa4
   % Motivo B y cadencia final
   sol8 do la8 do re do
   sib8 sol la2
}

chelo = \relative do {
   \global
   \clef bass
   %r8 la4 sib16 do do8 do
   r8  fa,8 do'16 sib la sol fa4
   la8^"I" mi^"V" mi'16 re do8 la8^"I" do
   %mib8 sib mi fa4 r8 2ª opción para el pedal
   % Motivo principal en el bajo
   % Imitación a la 4ª
   do16^"Imitación a la 4ª" mi' sol,8 si16 la sol8 mi do
   sol16 do sol sib sol la mi'8 do8 mi8
   % Cadencia
   fa4
   % Empieza 1er episodio
   % Motivo B
   sol8 la16 sib do4
   mi,8 re do8 sib16 mi do4
   % Fin de motivo B, empieza progresión
   fa16^"Progresión" re mi do re si dos16 fa16
   % Modulación y cadencia perfecta a rem
   re4^"rem y cadencia" 
   sib8_"II6/4" la_"I6/4" la'4_"V" re,4_"I"
   % Ritornello en rem
   % I - V - I
   r8 re8 la dos re8 fa
   % I - V - I
   re8 fa dos re16 mi fa8 re8
   re'16 sib, sol'8 mi16 fa sol8 sib re
   % Cadencia perfecta final de ritornello
   % II6/5  - V 5/4 - 5/3 - I
   sol,4 la8 la, re4 
   % Episodio 2.
   fa4 re4 sol8 re mi si do4^"Fin progresión"
   mi8^"Mot. B en dominante" re16 do si4 
   do8 re do16 re do8
   % Cadencia final episodio
   fa4 sol,4 do8. re16
   % Ritornello 3 (en dominante)
   do16^"Ritornello 3 (en DoM)" mi' sol,8 si16 la sol8 mi do
   sol16 do sol si sol la re8 do4
   % Estrechos y cadencia
   si do16 mi sol8 si16 la sol8 mi do
   % Cadencia
   <fa la>4 <sol~ do>8 <sol si> do4
   % Episodio 3
   fa,8^"Episodio 3, motivo B inv." sol16 la do4
   la8 fa8 sol8 sol16 fa sol4
   \clef tenor
   % Progresión y cadencia
   do4 si re
   do4 fa,2
   %do16 mi re fa mi sol fa la sol sib sib do16
   %fa,4 do8 mi fa4
   \clef bass
   % Ritornello 4 en FaM
   % Imito el principio para indicar final
   r8^"Ritornello 4"  fa8 do8 mi8 fa4
   la8 fa do16 fa sib,8 la8 do
   % Cabeza motivo B
   sib8 la16 sol fa4~
   % Cadencia final con pedal de tónica
   <fa~ sib>8^"Cadencia" <fa~ do'~>
   <fa~ do'>4 fa2
}

Partflauta = \new Staff \with {
  instrumentName = "Flauta"
  midiInstrument = "flute"
} \flauta

Partchelo = \new Staff \with {
  instrumentName = "Violonchelo"
  midiInstrument = "cello"
} \chelo

\score {
  <<
    \Partflauta
    \Partchelo
  >>
  \layout { }
  \midi {
    \tempo 4=80
  }
}
