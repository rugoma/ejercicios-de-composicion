% Lily was here -- automatically converted by /usr/bin/midi2ly from /home/razlobo/Medios/Damal/Proyectos/Varios/Conservatorio/ejercicios-de-composicion/6o_EP/004_Rock_Historias_de_ciudad_despierta.mid
\version "2.14.0"

\layout {
  \context {
    \Voice
    \remove "Note_heads_engraver"
    \consists "Completion_heads_engraver"
    \remove "Rest_engraver"
    \consists "Completion_rest_engraver"
  }
}

trackAchannelA = {

  \time 5/4

  \tempo 4 = 160

}

trackA = <<
  \context Voice = voiceA \trackAchannelA
>>


trackBchannelB = \relative c {
  \voiceOne
  d4 fis g d'2
  | % 2
  c,4 e g c2
  | % 3
  d,4 fis g d'2
  | % 4
  c,4 e g c2
  | % 5
  d,4 fis d' g,2
  | % 6
  fis4 c e c' g2 e4 g, g''8 d b g
  | % 8
  r8 ais,4. ais''8 f d ais f4
  | % 9
  d fis d' g,2
  | % 10
  fis4 c e c' g2 e4 g, g''8 d b g
  | % 12
  r8 ais,4. ais''8 f d ais f4
  | % 13
  d fis d' g,2
  | % 14
  fis4 c e c' g2 e4 g, g''8 d b g
  | % 16
  r8 ais,4. ais''8 f d ais f4
  | % 17
  d fis d' g,2
  | % 18
  fis4 c e c' g2 e4 g, <g'' d b g >8 <g d b g >4 <g, b d g >8
  | % 20
  <ais' f d ais f ais, > <ais,, f' ais d f ais > <ais f' ais d f ais >4*160/960
  r4*320/960 <ais'' f d ais f ais, >2 <f, ais d f ais >4 <d fis >
  <d fis >4*320/960 <d fis > <d fis > <g d' e >8 <e' d g, > <g, d' e >4
  <g d' e >8 <c, e >4
  | % 22
  <c e >4*320/960 <c e > <c e > <g' c e >8 <g c e >4 <g c e >8
}

trackBchannelC = \relative c {
  \voiceTwo
  r4*35 g8 r8*39 g8 r8*39 g8
}

trackB = <<

  \clef bass

  \context Voice = voiceA \trackBchannelB
  \context Voice = voiceB \trackBchannelC
>>


trackCchannelB = \relative c {
  \voiceThree
  r1*27 <g d' g >4 <g d' g >4*160/960 r4*80/960 <g d' g >8.
  | % 23
  <g d' g >8 <ais f' ais >4*320/960 <ais f' ais > <ais f' ais >8
  <ais f' ais >4 <d fis > <d fis >8 <d g >4*800/960
  | % 24
  <d fis >8 <d fis >4*640/960 <d fis > <d fis > <d g >4 <d fis >8
  <c e >4
  | % 25
  <e c >8 <c f >4 <c e >8 <c e >4*640/960 <c e > <c e > <c f >4
  | % 26
  <c e >8 a4 <g d' g > <g d' g >4*160/960 r4*80/960 <g d' g >8.
  <g d' g >8 <ais f' ais >4*320/960 <ais f' ais > <ais f' ais >
  | % 27
  <ais f' ais >8 <ais' f >4 <e, b' > <e b' >4*160/960 r4*80/960 <e b' >8.
  <b' e, >8 <f c' >4
  | % 28
  <f c' >4*160/960 r4*80/960 <f c' >8. <f c' >8 <ais f' > <f' ais, >
  <ais, f' > <f' ais, > <ais, f' > <f' ais, > <ais, f' >
  | % 29
  <f' ais, > r8 <a, e' >4*80/960 r4*400/960 <a e' >4*80/960 r4*400/960 <a e' >4*80/960
  r4*400/960 <e' a, >4*80/960 r4*400/960 <d a' >4 <a e' >4*80/960
  r4*400/960 <e' a, >4*80/960 r4*400/960
  | % 30
  <a, e' >4*80/960 r4*400/960 <a e' >4*80/960 r4*400/960 <d a' >4
  <g, d' >4*80/960 r4*400/960 <g d' >4*80/960 r4*400/960 <d' g, >4*80/960
  r4*400/960 <g, d' >4*80/960 r4*400/960 <c g' >4
  | % 31
  <d g, >4*80/960 r4*400/960 <g, d' >4*80/960 r4*400/960 <g d' >4*80/960
  r4*400/960 <g d' >4*80/960 r4*400/960 <c g' >4 <g d' >2
  | % 32
  <g d' >8 <a e' >2 <a e' >4*80/960 r4*400/960 <e' a, >4*80/960
  r4*400/960 <a, e' >4*80/960 r4*400/960 <a e' >4*80/960 r4*400/960 <d a' >4
  <a e' >4*80/960 r4*400/960 <e' a, >4*80/960 r4*400/960 <a, e' >4*80/960
  r4*400/960 <e' a, >4*80/960 r4*400/960 <d a' >4 <g, d' >4*80/960
  r4*400/960 <d' g, >4*80/960 r4*400/960 <g, d' >4*80/960 r4*400/960
  | % 34
  <d' g, >4*80/960 r4*400/960 <c g' >4 <g d' >2 r8*13 e'8 gis
  e a e gis e4 f8 a
  | % 37
  f ais f a4 d,8 fis d g d
  | % 38
  fis d4 <b dis >8 <b dis > <b dis > <b e > <b dis > <b e > b
  | % 39
  r8 e gis e a e gis e4 f8
  | % 40
  a f ais f a4 d,8 fis d g
  | % 41
  d fis d4 <c e >8 <c e > <c e > <c f > <c e > <c f >
  | % 42
  <c e >4 <c e >8 <c e > <e c > <c f > <c e > <c f > <c f >4
  | % 43
  a <g d' > r8 <a e' > <a e' >4 r4
  | % 44
  <e' gis > <e a >8 <e gis > <b dis >4 <b e >8 <b dis > <d fis >4
  | % 45
  <d g >8 <d fis > <d fis >4*160/960 r4*160/960 <d fis > r4*160/960 <d fis >
  r4*160/960 <d a' >4*320/960 <d fis > <d a' >4*1280/960
}

trackCchannelBvoiceB = \relative c {
  \voiceOne
  r8*379 dis4
}

trackCchannelC = \relative c {
  \voiceFour
  r8*261 ais4 r8*161 <g d' >8
}

trackC = <<

  \clef bass

  \context Voice = voiceA \trackCchannelB
  \context Voice = voiceB \trackCchannelBvoiceB
  \context Voice = voiceC \trackCchannelC
>>


trackDchannelB = \relative c {
  \voiceOne
  r4*11 e'4 d2 e4
  | % 4
  e g2 g8 r8 c4
  | % 5
  b a g2 a8 b
  | % 6
  a g4 r4 e d d8
  | % 7
  g fis d d b' a g d r1. b'4 a g2 a8 b
  | % 10
  a g4 r4 e d d8
  | % 11
  g fis d d b' a g d r1. b'4 a g2 a8 b
  | % 14
  a g4 r4 e d d8
  | % 15
  g fis d d b' a g d
}

trackDchannelC = \relative c {
  \voiceTwo
  r2*5 c'4 r8*15 g'8 r8*15 d4 r4*19 d4 r4*19 d4
}

trackD = <<
  \context Voice = voiceA \trackDchannelB
  \context Voice = voiceB \trackDchannelC
>>


trackEchannelB = \relative c {
  r4*113 fis4 a
  | % 24
  g
}

trackE = <<

  \clef bass

  \context Voice = voiceA \trackEchannelB
>>


\score {
  <<
    \context Staff=trackB \trackA
    \context Staff=trackB \trackB
    \context Staff=trackC \trackA
    \context Staff=trackC \trackC
    \context Staff=trackD \trackA
    \context Staff=trackD \trackD
    \context Staff=trackE \trackA
    \context Staff=trackE \trackE
  >>
  \layout {}
  \midi {}
}
