\version "2.19.82"
\language "espanol"

\header {
  title = "Gavota en SolM"
  composer = "R. Gómez Antolí"
  copyright = "CC-By 4.1"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key sol \major
  \time 2/2
  \partial 2
  \tempo "Allegro"
}

right = \relative do'' {
  \global
  % La música continúa aquí.
  <si re>4 <la do re>
  <re sol, si>4 <dos sol la~>4 <re la fas>8 <re, fas> <fas la dos>4
  <sol si re>4 <sol si>4 <sol si>4. <si re>8
  <<
    {re8[ si]}
    \\
    {la4}
  >>
  <la fas do'>4 <re, fas~ la>8 <mi fas> <fas do' re>4
  <sol si re>4 <fas la>4 <fas la re>2 % 1ª semifrase
  <fas la re>2 <mi sol la>4 <la dos mi>4
  <mi la sol>4 <re fas la> <mi re sol si>4 <dos mi la>4
  <re fas la>8[ <la' fas re>] <si sol re>[ <la fas re>]
  <<
     { re4. dos8}
     \\
     {si4 la}
     \\
     {mi2}
  >>
  %<re si mi,~>4. <dos la mi>8
  <re fas la>4 r4 % Final 2ª semifrase y 1ª parte (A)
  % 2ª parte
  <fas la re>4 <re fas la>
  <dos mi la>4 <sol' la mi'> <fas la re> <la fas re>
  <dos, mi sol> <mi la> <dos mi sol> <re fas la>
  <si re sol>4 <re fas la> <sol si mi> <mi sol si>
  <dos mi sol la>2 <fas si re>4 <fas la re,>
  <la re fas> <fas la re> <dos~ sol'~ la> <dos mi sol la>
  <fas re si> <dos mi la> <re la' fas> <sol, si mi>
  <re' fas la> <re fas si> <re mi sol si> <sol dos mi>
  <la re fas>4. r8
  % Modulación a mim
  <re, fas si>4 <mi sol si>
  <si res fas la> <mi sol si> <res fas si>2
  %
  <sol si mi>4 <mi sol si>4
  <mi sol si>8 <res fas si> <fas la dos mi> <fas si res> 
  <mi sol si>4 
  <fas si res> <sol si mi> <do la mi>4 <re fas si>4 r4 
  <sol, si mi>2 <si, mi sol>2
  <re fas si>4 <dos mi la> <si fas' la>2
  <mi sol si>4 <si mi sol> <la do mi> <mi' sol si>
  <res la' si> <fas la si> <sol si mi>4 r4
  % Modulación a SolM, progresión y final
  <mi sol si> <re fas la> <re~ sol~ si> <re sol re'>
  % Progresión
  <re la' re>8 <sol si re>8 <sol do>4 <fas la do> <fas si>
  <mi sol si> <mi la> <re fas la> <re sol si>
  <sol si re> <mi sol la> <re fas la>4.
  r8 r4 <re sol si>4 <si re sol> <re sol si>
  <mi sol si> <mi sol la> <mi sol si> <la re, re'>
  <re, fas la> <re fas la> <re sol si> <mi sol la>4 <si re sol>4
  <la re fas> <si re sol>2
}

left = \relative do' {
  \global
  % La música continúa aquí.
  % 1ª parte, dos semifrases de 4 compases
  sol4 fas sol mi re4. la8 si do re si si'4 la8 sol
  fas mi fas re do4 la4
  si8 do re4 mi8 re4. % Final 1ª semifrase
  fas4 re dos16 si la sol la4 
  dos re sol4. sol8
  fas16 mi re fas si,8 re sol,4 la re4. mi16 fas \bar ":..:"
  % 2ª parte: 8 compases + 8 + 4 + 4
  % ReM -> mim -> SolM -> progresión y final
  % 1ª subfrase 4 compases, reposo en semicadencia
  fas4 la la, dos4 re2
  mi8 la8 dos4 dos,4 re4
  si8 dos re8 fas
  sol8 mi8 sol8 si8 la8 mi8 dos la
  % 2ª subfrase, termina en cadencia perfecta
  % Empiezo en tónica
  si4 re4 fas4 la4
  mi4 dos4 re8 si la4
  re4 mi8 res re dos re4
  sol4 la,4 re4. do8
  % Siguientes 8 compases.
  % 1ª sufbrase, modulación a mim
  si4 mi4
  %% Compas 17
  fas4 sol,4 si8 res fas8 si8 % 7+
  sol2 mi8 res8 dos si8 mi4
  la,4 sol8 mi la8 do si4. do16 re % Semicadencia
  % Siguientes 4c en mim
  mi2 sol8 fas8 mi4
  si4 dos res2 % S S D
  mi4 sol,8 si do4 mi4 % T
  fas4 si,4 mi4. re16 fas % Cadencia perfecta
  %%% Modular a SolM en 1C, progresión de 3C con final en semicadencia
  mi4 re4 sol,2
  % Empiezo la progresión
  fas'8 sol mi4 fas
  re mi do re
  sol,4
  sol' do,4 re2
  %%% Final
  sol4 si4 si si,8 la8
  sol4 do4 mi fas
  re do si do re
  re8 re,8 sol2
}

cifrado = \figures {
  \figuremode {
    <_>4 <6 _5/> % Anacrusa
    <_> <6\+> <_>4. <6\+>8
    <6>4 <_>4 <6> <_>8 <5>8
    <6>2 <4\+>4 <_>4
    <6>4 <5> <_>8 <5>4.
    <6>4 <5>4 <6 _5/>2
    <6 _5/>4 <5> <6 _5> <4\+>
    <6> <6>8 <5> <6 _5>4 <4>8 <3>
    <5>4 <_> % Fin 1ª parte
    <6>4 <6 _4> <_>4 <6 _5/>4 <_>2
    <6\+>4 <6 _5/>2 <_>4
    <6>4 <5>4
    <6> <_> <5>4 <6>4 
    % Compas 13
    <5>4 <_> <6>4 <6 _4>4 
    <6\+> <_>4 <6> <7_\+>4
    <5> <_> 
    <5> <6> <6 _5>4 <7_\+>
    <5>2 <5>2
    <6\+>4 <6>4 <5 _+>4 <_>8 <6>8
    <6>2 <5>8 <6> <6\+> <5 _+> <_>4 <4\+>4
    <6>8 <5>8 <_>2.
    <_>2 <6>8 <_>4.
    <5>4 <6> <6 _5/>2
    <_>4 <6>8 <_> <6>4 <_>
    <6\+> <_>2.
    %% Modulación a SolM
    <_>4 <5> <_>2
    <6>8 <5> <6>4 <5> <6>
    <5> <6> <5> <_>4
    <_>4 <6 _5> <_>2.
    <6>4 <6> <6>8 <_>8 <6>4
    <6 _5>4 <5> <6> <_>
    <4\+>
    <6> <6 _5> <6 _4>
    <5 _3>
  }
}


violin = \relative do'' {
  \global
  % La música continúa aquí.
  re4. re,8 re16 mi fas sol la4 la16 sol fas4 re'8
  do16 si8. la16 sol8. re16 mi fas sol la8 si8
  la4 si16 do re8 re,4. mi8 
  re2 la'4 re4
  re8 dos16 si la8 fas sol4. la8
  sol16 fas mi sol fas4 mi16 re dos si mi4
  re'16 dos si la si8 dos re4. dos8 re2
  % 2ª parte: 8 compases + 8 + 4 + 4 = 24 compases en total
  % ReM -> mim -> SolM -> progresión y final
  % 1ª subfrase 4 compases, reposo en semicadencia
  la'4 re, 
  mi8 sol la4 sol16 fas mi re fas4
  sol8 mi re4 mi re8 fas
  re4 la8 re 
  si'4 sol mi2
  % Siguientes 8 compases. C13+
  % 1ª sufbrase, seguimos en ReM
  r8 fas re4 la re
  dos8 si la4 si sol
  fas mi4 fas4 si4.
  % Cadencia perfecta en ReM
  la8 sol4 fas8 la4.
  % modulación a mim
  % Siguientes 8c en mim
  re4 si8 dos res4 mi
  res fas8 mi mi4\prall sol8 fas
  sol8 fas la si sol4 si,
  % Semicadencia en mim, C-20
  mi,8 fas' mi8 re si4. r8
  % Sigo en mim 4C
  r4 mi si4 fas'8 mi
  fas4 la si8 la8 fas4
  mi res mi2
  si2 sol4 si8 mi,
  %%% Modular a SolM en 1C, progresión de 3C con final en semicadencia
  si'4 fas' re sol
  % Empiezo la progresión
  re4 mi fas si
  sol la do si
  sol la si la8 r8
  %%% Final
  r4 re mi8 fas sol4
  mi do sol fas
  re'2 si4 la sol
  fas sol2 \bar ":|."
}

pianoPart = \new PianoStaff \with {
  instrumentName = "Piano"
} <<
  \new Staff = "right" \with {
    midiInstrument = "acoustic grand"
  } \right
  \new Staff <<
    \new voice = "left" \with {
    midiInstrument = "acoustic grand"
    } { \clef bass \left }
    \cifrado
  >>
>>

violinPart = \new Staff \with {
  instrumentName = "Violín"
  midiInstrument = "violin"
} \violin

\score {
  <<
    \violinPart
    \pianoPart
  >>
  \layout { }
  \midi {
    \tempo 4=100
  }
}
