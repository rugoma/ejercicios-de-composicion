\version "2.20.0"
\language "espanol"

\header {
  title = "Coral 02 en solm"
  composer = "R. Gómez Antolí"
  copyright = "CC-BY"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key sol \minor
  \time 4/4
}

sopranoVoice = \relative do'' {
  \global
  \dynamicUp
  % La música continúa aquí.
  \stemUp
  re4 re la sib
  do sib8 la la4 sol\fermata
  sib sib la sol
  fas sol la2\fermata
  re4 re la sib
  do sib8 la la4 sol\fermata
  sib sib la sol
  fas sol la2\fermata
  la4 sib do la
  sib la8 sol sol4 fa\fermata
  sib do re re
  mib re8 do do4 sib\fermata
  re re do re
  sib do re2\fermata
  re4 re do sib
  la la sol2\fermata
}

verseSopranoVoice = \lyricmode {
  % La letra sigue aquí.
  
}

altoVoice = \relative do' {
  \global
  \dynamicUp
  % La música continúa aquí.
  \stemDown
  sol'8 la fa sol sol fas sol4
  fas sol~ sol8 fas re4
  mib8 fa re4 do re
  mib re8 mib fas2
  sol2 fas4~fas8 sol
  la4 sol~sol8 do, sib4
  lab'4 sol re8 mib re4
  %%
  do8 mib re4 fas2
  sol4 fa sol2
  fa4 sol re re
  mib8 fa mi4 fa fa
  sol fa mib8 sol fa4
  sib8 la sib fa la4 sib8 la
  sol fa mi la la4
  sib la8 sib la4 fas
  sol fas re2
}

verseAltoVoice = \lyricmode {
  % La letra sigue aquí.
  
}

tenorVoice = \relative do' {
  \global
  \dynamicUp
  % La música continúa aquí.
  \stemUp
  re2 mib4 sib8 sol
  la re~ re4 do sib
  sol mib8 fa mib fas sol re
  do' la re4 re2 % Primeros 4 compases
  sib2 re8 mib8 re4
  mib re fas,4 sol
  fa'8 fa, sol4 do sib8 sol
  do4 sib8 sol sol2 % Compas 8
  mib'8 la, re4 mib8 re mib8 do
  sib4 do8 la sib4~ sib
  sib la8 sol fa mib re re'
  sib do re4 mib re %Compas 12
  sol4 fa mib8 do sib4
  mib4 sol, fas2
  sol8 sol' la4 mib4 sib8 re
  mib re do4 si2
}

verseTenorVoice = \lyricmode {
  % La letra sigue aquí.
  
}

bassVoice = \relative do {
  \global
  \dynamicUp
  % La música continúa aquí.
  \stemDown
  sol2 do4 re
  re re re sol
  sol, sols la sib
  la sib8 do re2 % Compás 4
  sol,8 fa mib re re'4 sib
  la re re sol
  re mib8 mib, fas4 sol
  la sib re2 % Compás 8
  do4 re8 fa do4 do
  re mib sol8 sol, sib4
  sol la la sib
  sol sib sol sib % Compás 12
  sol4 sib do sol
  sol la re2
  sol,4 fas fa8 fa' re4
  do re sol,2
}

verseBassVoice = \lyricmode {
  % La letra sigue aquí.
}

figBass = \figuremode {
  \global
  % Las cifras van aquí.
  
}

%sopranoVoicePart = \new Staff \with {
%  instrumentName = "Soprano"
%  midiInstrument = "choir aahs"
%} { \sopranoVoice }
%\addlyrics { \verseSopranoVoice }

%altoVoicePart = \new Staff \with {
%  instrumentName = "Alto"
%  midiInstrument = "choir aahs"
%} { \altoVoice }
%\addlyrics { \verseAltoVoice }

%tenorVoicePart = \new Staff \with {
%  instrumentName = "Tenor"
%  midiInstrument = "choir aahs"
%} { \clef "treble_8" \tenorVoice }
%\addlyrics { \verseTenorVoice }

%bassVoicePart = \new Staff \with {
%  instrumentName = "Bajo"
%  midiInstrument = "choir aahs"
%} { \clef bass \bassVoice }
%\addlyrics { \verseBassVoice }

bassFiguresPart = \new FiguredBass \figBass

\score {
  \new ChoirStaff <<
    \new Staff <<
      \new Voice = "Soprano" {
        %\voiceOne
        \sopranoVoice
        %\addlyrics { \verseSopranoVoice }
      }
      \new Voice = "Contraalto" {
        %\voiceTwo
        \altoVoice
        %\addlyrics { \verseAltoVoice }
       }
    >>
    \new Staff <<
      \clef bass 
      \new Voice = "Tenor" {
        \voiceOne
        \tenorVoice
        \addlyrics { \verseTenorVoice }
      }
      \new Voice = "Bajo" {
        \voiceTwo
        \bassVoice
        \addlyrics { \verseBassVoice }
        \bassFiguresPart
      }
    >>
  >>
  \layout { }
  \midi {
      \tempo 4=70
  }
}
