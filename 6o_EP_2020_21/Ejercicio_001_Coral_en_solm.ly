\version "2.20.0"
\language "espanol"

\header {
  title = "Coral 01 en solm"
  composer = "R. Gómez Antolí"
  copyright = "CC-BY"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key sol \minor
  \time 4/4
}

sopranoVoice = \relative do'' {
  \global
  \dynamicUp
  % La música continúa aquí.
  sol4 la sib sib
  do sib la la\fermata
  sib do re re
  mib re do do
  do sib\fermata re do
  re re do sib
  do do\fermata do re
  mib mib re do8 re8
  re2 do2\fermata
  sol4 la sib sib
  do do re re\fermata
  re re mib re
  do sib8 la la2
  sol2 r2 \bar "|."
}

verseSopranoVoice = \lyricmode {
  % La letra sigue aquí.
  
}

altoVoice = \relative do' {
  \global
  \dynamicUp
  % La música continúa aquí.
  re8 mib re4 mib sib8 do16 re
  mib4 re do la
  re mib la sib8 la
  sib8 la sol4 fas sol
  la4 sib fa sol
  fas sol8 sib fa4 sib
  sib la mib fa
  sol8 mib la4 fa fa
  fa sol~sol2
  mi4 mi re fa
  mib fa8 mib fa4 sol
  la8 sib fa mib fa sol fa4
  sol8 fa sol re s2
  re2 r2 \bar "|."
}

verseAltoVoice = \lyricmode {
  % La letra sigue aquí.
  
}

tenorVoice = \relative do' {
  \global
  \dynamicUp
  % La música continúa aquí.
  sib4 do8 la sib la sol4
  sol8 la sib4 mib,4 fas
  sib sol8 do fas4 sol
  sol sib, mib8 re mib fa
  mib re re4 sib8 fa la4
  la sib do re8 sib
  sol4 fa mib8 sol sib do
  sib4 do re do8 si8
  la sol fa4 sol2
  do4 la8 mib sib' do re4
  do8 re mib4 re8 do sib4
  do4 sib do re8 fa
  mib4 re8 do8 s2
  si2 r2 \bar "|."
}

verseTenorVoice = \lyricmode {
  % La letra sigue aquí.
  
}

bassVoice = \relative do {
  \global
  \dynamicUp
  % La música continúa aquí.
  sol4 fas sol sib
  mib sol8 sol, la do re4
  sol8 fa mib4 do8 la sol4
  mib8 fas sol sib la4 sol
  fas4 mib sib' do8 mib
  re la sol4 la8 sol fa4
  mi4 fa do'4 sib8 la
  sol4 fa8 la sib4 la8 si
  do4 si do2
  do8 re do4 sol' fa8 fa,
  la4 la sib8 sol sib sol
  fas4 sib la sib
  mib sol8 fas s2
  sol,2 r2 \bar "|."
}

verseBassVoice = \lyricmode {
  % La letra sigue aquí.
  I V VI I
  IV I - II - V
  I - II V - I
  IV - I - VII IV
  VII I III II -
  V - I/VI V - I
  V/V V II I -
  IV V - I V/IV VII
  II V I
  III - II I VI -
  II V/III III V I -
  V III V/III III
  IV I V
  I
}

figBass = \figuremode {
  \global
  % Las cifras van aquí.
  <_>4 <6 _5> <6> <6>
  <6> <_> <_> <_> 
  <_> <6> <4\+> <_>
  <_> <_> <6> <_> 
  <_> <_> <_> <_> 
  <_> <_> <6> <6 _4>
  <_> <_> <6> <_>
  <6> <7\+> <_> <_> 
  <6 _5> <6 _5/> <_>2
  <_>4 <_>  <_>  <6 _4>
  <_>  <6 _5> <_> <6>
  <6 _5> <_>  <_>  <_> 
  
}

sopranoVoicePart = \new Staff \with {
  instrumentName = "Soprano"
  midiInstrument = "choir aahs"
} { \sopranoVoice }
\addlyrics { \verseSopranoVoice }

altoVoicePart = \new Staff \with {
  instrumentName = "Alto"
  midiInstrument = "choir aahs"
} { \altoVoice }
\addlyrics { \verseAltoVoice }

tenorVoicePart = \new Staff \with {
  instrumentName = "Tenor"
  midiInstrument = "choir aahs"
} { \clef bass \tenorVoice }
\addlyrics { \verseTenorVoice }

bassVoicePart = \new Staff \with {
  instrumentName = "Bajo"
  midiInstrument = "choir aahs"
} { \clef bass \bassVoice }
\addlyrics { \verseBassVoice }

bassFiguresPart = \new FiguredBass \figBass

\score {
  <<
    \sopranoVoicePart
    \altoVoicePart
    \tenorVoicePart
    \bassVoicePart
    \bassFiguresPart
  >>
  \layout { }
  \midi {
    \tempo 4=64
  }
}
